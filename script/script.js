$(document).ready(function () {

    // Setup the sliders 
    var partnerCarousel = $('#partner-carousel');
    partnerCarousel.owlCarousel({
        items: 4,
        loop: true,
        margin: 10,
        autoplay: true,
        autoplayTimeout: 1500,
        autoplayHoverPause: true,
        loop: true,
        nav: true,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 4
            },
            1000: {
                items: 4
            }
        }
    });

    var newsCarousel = $('#news-carousel');
    newsCarousel.owlCarousel({
        items: 3,
        loop: true,
        margin: 10,
        autoplay: true,
        autoplayTimeout: 1500,
        autoplayHoverPause: true,
        loop: true,
        nav: true,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 3
            },
            1000: {
                items: 3
            }
        }
    });

    var courseSlider = $('#course-slider');
    courseSlider.owlCarousel({
        items: 3,
        loop: true,
        margin: 10,
        autoplay: true,
        autoplayTimeout: 1500,
        autoplayHoverPause: true,
        loop: true,
        nav: true,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 3
            },
            1000: {
                items: 3
            }
        }
    });

    // Remove unnecessaries
    $('.owl-nav').remove();
    $('.owl-dots').remove();


    // Show the news at the topbar
    var curIndex = 0;
    $('.article-link').eq(0).slideDown();

    function showLink() {
        if (curIndex == $('.article-link').length - 1) {
            $('.article-link').eq(curIndex).hide();
            curIndex = 0;
            $('.article-link').eq(curIndex).slideDown('slow');
        } else {
            $('.article-link').eq(curIndex).hide();
            curIndex++;
            $('.article-link').eq(curIndex).slideDown('slow');
        }
    }
    setInterval(function () {
        showLink();
    }, 2000);

    // Count up on the first time
    var firstTimeCount = true;

    function isScrolledIntoView(elem) {
        var docViewTop = $(window).scrollTop();
        var docViewBottom = docViewTop + $(window).height();
        var elemTop = $(elem).offset().top;
        var elemBottom = elemTop + $(elem).height();
        return ((elemBottom >= docViewTop) && (elemTop <= docViewBottom) && (elemBottom <= docViewBottom) && (elemTop >= docViewTop));
    }

    $(window).scroll(function () {
        if (firstTimeCount && isScrolledIntoView($('#statistics'))) {
            $('.count').each(function () {
                var $this = $(this);
                jQuery({
                    Counter: 0
                }).animate({
                    Counter: $this.attr('data-stop')
                }, {
                    duration: 2500,
                    easing: 'swing',
                    step: function (now) {
                        $this.text(Math.ceil(now));
                    }
                });
            });

            firstTimeCount = false;
        }
    });

    // Fixed navbar
    var firstTimeNavbar = true;
    // When the user scrolls the page, execute myFunction 
    window.onscroll = function () {
        myFunction()
    };

    // Get the navbar
    var navbar = document.getElementById("wrapper-navbar");

    // Get the offset position of the navbar
    var sticky = navbar.offsetHeight;

    // Add the sticky class to the navbar when you reach its scroll position. Remove "sticky" when you leave the scroll position
    function myFunction() {
        if (window.pageYOffset >= sticky) {
            navbar.classList.add("sticky");
            if (firstTimeNavbar) {
                $('#wrapper-navbar').hide();
                $('#wrapper-navbar').slideDown('slow');
                firstTimeNavbar = false;
            }
        } else {
            navbar.classList.remove("sticky");
            firstTimeNavbar = true;
        }
    }

    // Reviews section
    hideAllReviews();
    showReview(1);
    function hideAllReviews() {
        $.each($('.review-content'), function (index, value) {
            $('.review-content').eq(index).hide();
        });
        $.each($('.review-client'), function (index, value) {
            $('.review-client').eq(index).hide();
        });
        $.each($('.client-avatar'), function (index, value) {
            $('.client-avatar').eq(index).removeClass('highlight');
        });
    }

    function showReview(index) {
        $('.review-content').eq(index).fadeIn('slow');
        $('.review-client').eq(index).fadeIn('slow');
        $('.client-avatar').eq(index).addClass('highlight');
    }

    $.each($('.client-avatar'), function (index, value) {
        $('.client-avatar').eq(index).click(function() {
            hideAllReviews();
            showReview(index);
        });
    });
});