'uses strict';

// Setup the slider
var instructorSlider = $('#instructor-slider');
instructorSlider.owlCarousel({
    items: 2,
    loop: true,
    margin: 10,
    autoplay: true,
    autoplayTimeout: 1500,
    autoplayHoverPause: true,
    loop: false,
    nav: true,
    responsive: {
        0: {
            items: 1
        },
        600: {
            items: 2
        },
        1000: {
            items: 2
        }
    }
});

var studentReviewSlider = $('#student-review-slider');
studentReviewSlider.owlCarousel({
    items: 4,
    loop: true,
    margin: 10,
    autoplay: true,
    autoplayTimeout: 1500,
    autoplayHoverPause: true,
    loop: true,
    nav: true,
    responsive: {
        0: {
            items: 1
        },
        600: {
            items: 3
        },
        1000: {
            items: 3
        }
    }
});

// Show the news at the topbar
var curIndex = 0;
$('.article-link').eq(0).slideDown();

function showLink() {
    if (curIndex == $('.article-link').length - 1) {
        $('.article-link').eq(curIndex).hide();
        curIndex = 0;
        $('.article-link').eq(curIndex).slideDown('slow');
    } else {
        $('.article-link').eq(curIndex).hide();
        curIndex++;
        $('.article-link').eq(curIndex).slideDown('slow');
    }
}
setInterval(function () {
    showLink();
}, 2000);

// Remove unnecessaries
$('.owl-nav').remove();
$('.owl-dots').remove();

 // Fixed navbar
var firstTimeNavbar = true;
// When the user scrolls the page, execute myFunction 
window.onscroll = function () {
    myFunction()
};

// Get the navbar
var navbar = document.getElementById("wrapper-navbar");

// Get the offset position of the navbar
var sticky = navbar.offsetHeight;

// Add the sticky class to the navbar when you reach its scroll position. Remove "sticky" when you leave the scroll position
function myFunction() {
    if (window.pageYOffset >= sticky) {
        navbar.classList.add("sticky");
        if (firstTimeNavbar) {
            $('#wrapper-navbar').hide();
            $('#wrapper-navbar').slideDown('slow');
            firstTimeNavbar = false;
        }
    } else {
        navbar.classList.remove("sticky");
        firstTimeNavbar = true;
    }
}